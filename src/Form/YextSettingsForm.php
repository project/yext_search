<?php

namespace Drupal\yext_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure freeman settings for this site.
 */
class YextSettingsForm extends ConfigFormBase {
  // @var string Config settings
  const SETTINGS = 'yext_search.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yext_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Yext search configuration.
    $this->yextSearchSettings($form, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Function to add program inquiry form settings.
   */
  protected function yextSearchSettings(array &$form, $config) {

    $form['yext_search'] = [
      '#type' => 'details',
      '#title' => $this->t('Yext Search Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
    ];

    $form['yext_search']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API KEY'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['yext_search']['experience_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Experience Key'),
      '#default_value' => $config->get('experience_key'),
    ];

    $form['yext_search']['experience_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Experience version'),
      '#default_value' => $config->get('experience_version'),
    ];

    $form['yext_search']['business_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Business Id'),
      '#default_value' => $config->get('business_id'),
    ];
    $form['yext_search']['locale'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Locale'),
      '#default_value' => $config->get('locale'),
    ];
    $form['yext_search']['redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect Url'),
      '#default_value' => $config->get('redirect_url'),
    ];
    $form['yext_search']['search_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Placeholder'),
      '#default_value' => $config->get('search_placeholder'),
    ];
    $form['yext_search']['yext_answers'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Yext Answers'),
      '#default_value' => $config->get('yext_answers'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get form state.
    $form_values = $form_state->getValues();

    // Remove unwanted values.
    unset($form_values['tokens']);
    unset($form_values['submit']);
    unset($form_values['form_build_id']);
    unset($form_values['form_token']);
    unset($form_values['form_id']);
    unset($form_values['op']);

    // Retrieve the configuration.
    $staples_config = $this->configFactory->getEditable(static::SETTINGS);

    // Set the submitted configuration setting.
    foreach ($form_values as $key => $value) {
      $staples_config->set($key, $value);
    }

    $staples_config->save();

    parent::submitForm($form, $form_state);
  }

}
