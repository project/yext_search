<?php

namespace Drupal\yext_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'yext search bar' block.
 *
 * @Block(
 *   id = "yextsearchbar_block",
 *   admin_label = @Translation("Yext Search Bar"),
 *
 * )
 */
class YextSearchBar extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $html = '<div class="search_form"></div>';
    return [
      '#markup' => $html,
    ];
  }

}
