<?php

namespace Drupal\yext_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'yext search result' block.
 *
 * @Block(
 *   id = "yextsearchresult_block",
 *   admin_label = @Translation("Yext Search Result"),
 *
 * )
 */
class YextSearchResult extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactory $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->configFactory->get('yext_search.settings');
    $tag = '<div id="answers-container"></div><script src="' . $config->get('yext_answers') . '"></script>';

    return [
      '#markup' => $tag,
      '#allowed_tags' => ['script', 'div'],
    ];
  }

}
