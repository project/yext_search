INTRODUCTION
------------
This module provide an integration of yext search.

The Yext Search Experience Cloud is a Software as a Service (SaaS) product suite that puts
businesses in control of their facts online. By providing official answers on their
own websites — then across search engines, maps, apps, voice assistants and chatbots
— Yext helps businesses answer those questions everywhere consumers search.

* For a full description of the module visit:
  https://www.drupal.org/project/yext_search
  https://www.yext.com/


REQUIREMENTS
------------

Need to create an account on yext.com.

INSTALLATION
------------

* Install the Yext Search module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
------------
After successfully installing the module yext search,

1. Navigate admin/config/yext/settings and add yext search settings. 
2. you can place 'Yext Search Bar' and 'Yext Search Result' block on region. 
  admin -> structure -> block -> Place 'Yext Search Bar' and 'Yext Search Result' on region. 

MAINTAINERS
-----------

Current maintainers:
* Sumit kumar (https://www.drupal.org/u/bsumit5577)
